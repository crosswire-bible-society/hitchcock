#!/bin/bash
wget https://ccel.org/ccel/h/hitchcock/bible_names/cache/bible_names.txt

rm hitchcock.tei.xml
cp bible_names.txt hitchcock.tei.xml

##Suppression retour chariot dos
sed -i '7932,7948d' hitchcock.tei.xml
sed -i '1,30d' hitchcock.tei.xml


##Entrée dic

sed -ri 's/^   ([A-Z][a-z_-]*)$/<\/entryFree>\n<entryFree n=\"\1\">/g' hitchcock.tei.xml

#def
sed -ri 's/          (.*)$/<def>\1<\/def>/g'  hitchcock.tei.xml
sed -ri 's/     __*$//g' hitchcock.tei.xml

sed -i s'/&#8211;/→ /g' hitchcock.tei.xml
cat header hitchcock.tei.xml close >temp
rm hitchcock.tei.xml
mv temp hitchcock.tei.xml
sed -i '27d' hitchcock.tei.xml

##COrrection diverses erreurs
sed -ri 's/(2<\/ref>)(Ch 36:12)/\1 <ref osisRef="2Chr\.36\.12">2 \2<\/ref>/g' hitchcock.tei.xml
xmllint --noout --schema http://www.crosswire.org/OSIS/teiP5osis.2.5.0.xsd hitchcock.tei.xml
mkdir ~/.sword/modules/lexdict/zld/hitchocknv/
tei2mod ~/.sword/modules/lexdict/zld/hitchocknv/ hitchcock.tei.xml -z
